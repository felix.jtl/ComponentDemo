<?php declare(strict_types=1);

namespace Template\ComponentDemo;

use scc\components\CSRFToken;
use scc\DefaultComponentRegistrator;

/**
 * Class MyCustomComponentRegistrator
 * @package Template\ComponentDemo
 */
class MyCustomComponentRegistrator extends DefaultComponentRegistrator
{
    public function registerComponents(): void
    {
        parent::registerComponents();
        foreach ($this->components as $component) {
            if (\get_class($component) === CSRFToken::class) {
                $component->setTemplate(__DIR__ . '/customCsrfTokenTemplate.tpl');
            }
        }
    }
}
